import React, { useState } from "react";
import { Button } from "react-bootstrap";
// import { connect, useDispatch, useSelector } from "react-redux";
// import { getApplicationStatus } from "./../redux/actions/index";
import PolicyModal from "./PolicyModal";

const Policy = (props) => {
	return (
		<>
			<tr>
				{console.log(props)}
				<th>{props.policy.policyNumber}</th>
				<td>{props.policy.premium}</td>
				<td>{props.policy.effectiveDate}</td>
				<td>{props.policy.product}</td>
				<td>
					<PolicyModal
						isEdit={true}
						policy={props.policy}
						onSavePolicy={(policy) => {
							props.onSavePolicy(policy);
						}}
					/>
					{/* <input type="button" onClick={props.onEdit} value="edit" /> */}
				</td>
				<td>
					<input type="button" onClick={props.onDelete} value="delete" />
				</td>
			</tr>
		</>
	);
};

const PolicyList = (props) => {
	return (
		<>
			{console.log(props)}
			{props.policyList.length > 0 && (
				<div className="table-responsive">
					<table className="table">
						<thead>
							<tr>
								<th scope="col">Policy Number</th>
								<th scope="col">Premium</th>
								<th scope="col">Effective Date</th>
								<th scope="col">Product Type</th>
								<th scope="col">...</th>
								<th scope="col">...</th>
							</tr>
						</thead>
						<tbody>
							{props.policyList.map((policy, index) => (
								<Policy
									key={index}
									policy={policy}
									onSavePolicy={(policy) => props.onSavePolicy(policy, index)}
									onDelete={() => props.onDelete(index)}
								/>
							))}
						</tbody>
					</table>
				</div>
			)}
		</>
	);
};

export default PolicyList;
