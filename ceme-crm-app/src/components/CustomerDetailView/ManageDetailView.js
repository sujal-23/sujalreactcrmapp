import React, {useEffect, useState } from 'react';
import CustomerPersonalInfo from './CustomerPersonalInfo';
import CustomerPolicyInfo from './CustomerPolicyInfo';
import { useDispatch, useSelector } from "react-redux";
import { getCustomerDetail } from "../../redux/actions/getCustDetailActions";
import {  Link} from 'react-router-dom';

import "./customerDetail.css";


const ManageDetailView = () => {

    const { customer } = useSelector((state) => state.search);
    const { custDetails, loading, error } = useSelector((state) => state.custDetail);
    
   console.log("customer", customer);
    const dispatch = useDispatch();
    const[custId, setCustId] = useState(customer.id);

    useEffect(() => {
     console.log("customer details....");
     
  
     console.log("customer id", custId);
        if(customer.id)
        {
             dispatch(getCustomerDetail(customer.id));
             
             console.log("customer details....");
             console.log(custDetails);
             console.log(loading);
             console.log(error);
        }
        else
        {
            setCustId(customer.id);
        }
        

    },[custId, customer.id, dispatch]);

   

    return(
        <div className="row">
            <div className="col-md-12 text-center">
            
                <h4 style={{color: "#404b5f"}}> {`Customer Detail for ${customer.lastName}, ${customer.firstName}.`} </h4>
           <br>
           </br>
            </div>

            <div className="col mod-12 text-right">
       
            <button type="button" className="btn btn-primary"><Link  to = {"/"}>Back to Home</Link></button>
            
            {"  "}
            <button type="button" className="btn btn-primary"><Link to = {"/viewInteraction/" + custId}>View Interactions</Link></button>
            {"  "}
            
            <button type="button" className="btn btn-primary"> <Link to = {"/addcustomer"}>Manage Details</Link></button>
              
              <br>
              </br>
                </div>
                <br></br>
            <CustomerPersonalInfo customer={customer} />
                <div style={{height:"19px"}}>

                </div>
           
         {<CustomerPolicyInfo custDetails={custDetails} loading={loading} error={error} />}
         <div style={{height:"19px"}}>

</div>
        
        </div>


    );

}


export default ManageDetailView;