import React, { useState, useEffect } from "react";
import { Form, Button, Col } from "react-bootstrap";
import PolicyModal from "./PolicyModal";
import PolicyList from "./PolicyList";
import { useSelector, useDispatch } from "react-redux";
import { saveCustomer } from "./../redux/actions/saveActions";
import { useHistory } from "react-router-dom";

import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

function AddCustomerForm(props) {
	const [validated, setValidated] = useState(false);

	const customerData = {
		id: 0,
		firstName: "",
		lastName: "",
		age: 16,
		gender: "",
		email: "",
		phone: "",
		address: "",
		state: "",
		zipcode: "",
		ssn: "",
		language: "",
	};

	const interaction = {
		id: 0,
		custId: 0,
		notes: "test note 1",
		phone: "(541) 754-3010",
		policyNumber: "9876541344",
		createdBy: "sujal 2",
		createdDate: "01/12/2020 10:09:30",
	};

	const [customer, setCustomer] = useState(customerData);

	const [policyList, setPolicyList] = useState([]);

	const { saveStatus } = useSelector((state) => state.save);
	const dispatch = useDispatch();
	const history = useHistory();

	useEffect(() => {
		if (props.isEdit === true) {
			setCustomer(props.customer);
			setPolicyList(props.policies);
		}
	}, []);

	useEffect(() => {
		if (saveStatus === true) {
			toast("Customer intraction saved successfully");
			setCustomer(customerData);
			setPolicyList([]);
			setValidated(false);
			//history.push("/");
		}
	}, [saveStatus]);

	const handleSubmit = (event) => {
		const form = event.currentTarget;
		if (form.checkValidity() === false) {
			event.preventDefault();
			event.stopPropagation();
		} else {
			let saveCustomerObj = {
				customer: customer,
				policies: policyList,
				interactons: interaction,
			};
			formatRequest(saveCustomerObj);
			dispatch(saveCustomer(saveCustomerObj));
			event.preventDefault();
		}
		setValidated(true);
	};
	// useEffect(() => {
	// 	console.log(policyList);
	// }, [policyList]);

	const changeHandler = (event) => {
		switch (event.target.name) {
			case "phone":
				event.target.value = formatPhoneNumber(event.target.value);
				break;
		}
		setCustomer({ ...customer, [event.target.name]: event.target.value });
	};

	function formatPhoneNumber(phoneNumberString) {
		var cleaned = ("" + phoneNumberString).replace(/\D/g, "");
		var match = cleaned.match(/^(\d{3})(\d{3})(\d{4})$/);
		if (match) {
			return "(" + match[1] + ") " + match[2] + "-" + match[3];
		}
		return phoneNumberString;
	}

	const onDeletePolicy = (index) => {
		const newList = policyList.filter((p, i) => i !== index);
		setPolicyList(newList);
	};

	const onSavePolicy = (policy, index) => {
		const newList = [];
		policyList.map((p, i) => {
			if (i === index) {
				newList.push(policy);
			} else {
				newList.push(p);
			}
			return newList;
		});
		setPolicyList(newList);
	};

	const formateDate = (date) => {
		var d = new Date(date);
		var datestring =
			d.getDate() +
			"/" +
			(d.getMonth() + 1) +
			"/" +
			d.getFullYear() +
			" " +
			d.getHours() +
			":" +
			d.getMinutes() +
			":" +
			d.getSeconds();
		return datestring;
	};

	const formatRequest = (request) => {
		request.customer.age = Number(request.customer.age);
		// request.customer.phone = request.customer.phone
		request.policies.forEach((policy) => {
			policy.effectiveDate = formateDate(policy.effectiveDate);
		});
		request.interactons.phone = request.customer.phone;
		request.interactons.policyNumber = request.policies[0].policyNumber;

		request.interactons.createdBy = request.customer.firstName;
		request.interactons.createdDate = formateDate(new Date());
	};

	// const isValidName = (e) => {
	// 	if (e.target.value.match("^[a-zA-Z ]*$") != null) {
	// 		return true;
	// 	}
	// 	return false;
	// };

	return (
		<Form noValidate validated={validated} onSubmit={handleSubmit}>
			<ToastContainer />
			<Form.Row>
				<Form.Group as={Col} md="4" controlId="firstName" className="text-left">
					<Form.Label>First name</Form.Label>
					<Form.Control
						required
						type="text"
						name="firstName"
						placeholder="First name"
						value={customer.firstName}
						pattern="[a-zA-Z]*"
						minLength="5"
						maxLength="15"
						onChange={(e) => changeHandler(e)}
					/>
					<Form.Control.Feedback type="invalid">
						Please enter a valid first name
					</Form.Control.Feedback>
				</Form.Group>
				<Form.Group as={Col} md="4" controlId="lastName" className="text-left">
					<Form.Label>Last name</Form.Label>
					<Form.Control
						required
						type="text"
						name="lastName"
						minLength="5"
						maxLength="15"
						placeholder="Last name"
						pattern="[a-zA-Z]*"
						value={customer.lastName}
						onChange={(e) => changeHandler(e)}
					/>
					<Form.Control.Feedback type="invalid">
						Please enter a valid last name
					</Form.Control.Feedback>
				</Form.Group>
				<Form.Group as={Col} md="4" controlId="ssn" className="text-left">
					<Form.Label>SSN</Form.Label>
					<Form.Control
						required
						type="text"
						name="ssn"
						minLength="9"
						maxLength="9"
						placeholder="ssn number"
						pattern="[0-9]*"
						value={customer.ssn}
						onChange={(e) => changeHandler(e)}
					/>
					<Form.Control.Feedback type="invalid">
						Please enter a valid last ssn
					</Form.Control.Feedback>
				</Form.Group>
			</Form.Row>
			<Form.Row>
				<Form.Group as={Col} md="4" controlId="age" className="text-left">
					<Form.Label>Age</Form.Label>
					<Form.Control
						required
						type="text"
						name="age"
						placeholder="Age"
						value={customer.age}
						pattern="[0-9]*"
						minLength="2"
						maxLength="2"
						onChange={(e) => changeHandler(e)}
					/>
					<Form.Control.Feedback type="invalid">
						Please enter a valid age
					</Form.Control.Feedback>
				</Form.Group>
				<Form.Group as={Col} md="4" controlId="gender" className="text-left">
					<Form.Label>Gender</Form.Label>
					<Form.Control
						as="select"
						name="gender"
						required
						value={customer.gender}
						onChange={(e) => changeHandler(e)}
					>
						<option value="">Select</option>
						<option value="M">Male</option>
						<option value="F">Female</option>
					</Form.Control>
					<Form.Control.Feedback type="invalid">
						Please select the gender
					</Form.Control.Feedback>
				</Form.Group>
				<Form.Group as={Col} md="4" controlId="language" className="text-left">
					<Form.Label>Language</Form.Label>
					<Form.Control
						as="select"
						name="language"
						required
						value={customer.language}
						onChange={(e) => changeHandler(e)}
					>
						<option value="">Select</option>
						<option value="english">English</option>
						<option value="spanish">Spanish</option>
					</Form.Control>
					<Form.Control.Feedback type="invalid">
						Please select the language
					</Form.Control.Feedback>
				</Form.Group>
			</Form.Row>

			<Form.Row>
				<Form.Group as={Col} md="4" controlId="email" className="text-left">
					<Form.Label>Email address</Form.Label>
					<Form.Control
						type="email"
						placeholder="Enter email"
						required
						name="email"
						value={customer.email}
						maxLength="50"
						onChange={(e) => changeHandler(e)}
					/>

					<Form.Control.Feedback type="invalid">
						Please enter a valid mail Id
					</Form.Control.Feedback>
				</Form.Group>
				<Form.Group as={Col} md="3" controlId="phone" className="text-left">
					<Form.Label>Contact Number</Form.Label>
					<Form.Control
						type="text"
						placeholder="Phone number"
						// pattern="[([(])([0-9]{3})([)])([-])([0-9]{3})([-])([0-9]{4})]"
						name="phone"
						value={customer.phone}
						onChange={(e) => changeHandler(e)}
						maxLength="14"
						minLength="14"
						required
					/>

					<Form.Control.Feedback type="invalid">
						Please enter a valid contact number
					</Form.Control.Feedback>
				</Form.Group>
				<Form.Group
					as={Col}
					md="5"
					controlId="addressLine1"
					className="text-left"
				>
					<Form.Label>Address </Form.Label>
					<Form.Control
						type="text"
						placeholder="house no, street address"
						required
						// pattern="[^0-9a-zA-Z]*"
						name="address"
						value={customer.address}
						onChange={(e) => changeHandler(e)}
						minLength="5"
						maxLength="25"
					/>
					<Form.Control.Feedback type="invalid">
						Please provide a valid address.
					</Form.Control.Feedback>
				</Form.Group>
			</Form.Row>
			<Form.Row></Form.Row>
			<Form.Row>
				<Form.Group as={Col} md="4" controlId="city" className="text-left">
					<Form.Label>City</Form.Label>
					<Form.Control
						name="city"
						type="text"
						placeholder="City"
						required
						pattern="[a-zA-Z]*"
						value={customer.city}
						onChange={(e) => changeHandler(e)}
						minLength="3"
						maxLength="15"
					/>
					<Form.Control.Feedback type="invalid">
						Please provide a valid city.
					</Form.Control.Feedback>
				</Form.Group>
				<Form.Group as={Col} md="4" controlId="city" className="text-left">
					<Form.Label>State</Form.Label>
					<Form.Control
						as="select"
						name="state"
						type="l"
						placeholder="State"
						required
						pattern="[a-zA-Z]"
						value={customer.state}
						onChange={(e) => changeHandler(e)}
					>
						<option value="">Select</option>
						<option value="AL">Alabama</option>
						<option value="AK">Alaska</option>
						<option value="AZ">Arizona</option>
						<option value="AR">Arkansas</option>
						<option value="CA">California</option>
						<option value="CO">Colorado</option>
						<option value="CT">Connecticut</option>
						<option value="DE">Delaware</option>
						<option value="DC">District Of Columbia</option>
						<option value="FL">Florida</option>
						<option value="GA">Georgia</option>
						<option value="HI">Hawaii</option>
						<option value="ID">Idaho</option>
						<option value="IL">Illinois</option>
						<option value="IN">Indiana</option>
						<option value="IA">Iowa</option>
						<option value="KS">Kansas</option>
						<option value="KY">Kentucky</option>
						<option value="LA">Louisiana</option>
						<option value="ME">Maine</option>
						<option value="MD">Maryland</option>
						<option value="MA">Massachusetts</option>
						<option value="MI">Michigan</option>
						<option value="MN">Minnesota</option>
						<option value="MS">Mississippi</option>
						<option value="MO">Missouri</option>
						<option value="MT">Montana</option>
						<option value="NE">Nebraska</option>
						<option value="NV">Nevada</option>
						<option value="NH">New Hampshire</option>
						<option value="NJ">New Jersey</option>
						<option value="NM">New Mexico</option>
						<option value="NY">New York</option>
						<option value="NC">North Carolina</option>
						<option value="ND">North Dakota</option>
						<option value="OH">Ohio</option>
						<option value="OK">Oklahoma</option>
						<option value="OR">Oregon</option>
						<option value="PA">Pennsylvania</option>
						<option value="RI">Rhode Island</option>
						<option value="SC">South Carolina</option>
						<option value="SD">South Dakota</option>
						<option value="TN">Tennessee</option>
						<option value="TX">Texas</option>
						<option value="UT">Utah</option>
						<option value="VT">Vermont</option>
						<option value="VA">Virginia</option>
						<option value="WA">Washington</option>
						<option value="WV">West Virginia</option>
						<option value="WI">Wisconsin</option>
						<option value="WY">Wyoming</option>
					</Form.Control>
					<Form.Control.Feedback type="invalid">
						Please select a valid state.
					</Form.Control.Feedback>
				</Form.Group>
				<Form.Group as={Col} md="4" controlId="zipcode" className="text-left">
					<Form.Label>Zip</Form.Label>
					<Form.Control
						name="zipcode"
						type="text"
						placeholder="Zip"
						required
						pattern="[0-9]*"
						value={customer.zipcode}
						onChange={(e) => changeHandler(e)}
						minLength="5"
						maxLength="5"
					/>
					<Form.Control.Feedback type="invalid">
						Please provide a valid zip.
					</Form.Control.Feedback>
				</Form.Group>
			</Form.Row>

			<Form.Row>
				<Form.Group className="text-left">
					<PolicyList
						policyList={policyList}
						onSavePolicy={(policy, index) => onSavePolicy(policy, index)}
						onDelete={(index) => onDeletePolicy(index)}
					></PolicyList>
				</Form.Group>
			</Form.Row>
			<Form.Group className="text-left">
				<PolicyModal
					policyList={policyList}
					onSavePolicy={(policy) => {
						setPolicyList(policyList.concat(policy));
					}}
				/>
			</Form.Group>
			<Form.Group className="text-left">
				<Form.Check
					required
					label="Agree to terms and conditions"
					feedback="You must agree before submitting."
				/>
			</Form.Group>
			<Button type="submit" className="text-left">
				Save Customer
			</Button>
		</Form>
	);
}
function AddCustomerPage(props) {
	return (
		<div className="row jumbotron">
			<div className="col-lg-12">
				<AddCustomerForm />
			</div>
		</div>
	);
}

export default AddCustomerPage;
