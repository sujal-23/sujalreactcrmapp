import React, { useEffect } from "react";
import { useSelector } from "react-redux";
import SearchCustomerAccordion from "./SearchCustomerAccordion";
import { useHistory } from "react-router-dom";
import { useDispatch } from "react-redux";
import { setSelectedCustomer } from "../../redux/actions/searchActions";
import PageviewIcon from "@material-ui/icons/Pageview";
import "./search.css";

const SearchResults = () => {
  const dispatch = useDispatch();
  const history = useHistory();
  const customers = useSelector((state) => state.search.data.customers);
  const searchText = useSelector((state) => state.search.data.searchText);
  const isSearchSuccess = useSelector((state) => state.search.data.isSuccess);
  const loading = useSelector((state) => state.search.loading);
  const error = useSelector((state) => state.search.data.error);
  const isSearchedButtonClicked = useSelector(
    (state) => state.search.isSearchedButtonClicked
  );

  const redirectToDetailsPage = (customer) => {
    dispatch(setSelectedCustomer(customer));
    history.push("/viewCustomer");
  };

  useEffect(() => {
    if (customers.length === 1 && isSearchedButtonClicked) {
      redirectToDetailsPage(customers[0]);
    }
  }, [customers, isSearchedButtonClicked]);

  if (loading) {
    return (
      <div className="d-flex justify-content-center">
        <div
          className="spinner-border m-5"
          style={{ width: "4rem", height: "4rem" }}
          role="status"
        >
          <span className="sr-only">Loading...</span>
        </div>
      </div>
    );
  }

  if (error && error.message) {
    return (
      <div className="alert alert-warning fade show">
        <h4 className="alert-heading">
          <i className="fa fa-warning"></i> {error.message}
        </h4>
      </div>
    );
  }

  return (
    <React.Fragment>
      {isSearchSuccess && (
        <div>
          <div className="d-flex p-2">
            <PageviewIcon />
            <span>
              <b> {customers.length} </b> search results found for{" "}
              <b>{searchText}</b>
            </span>
          </div>
          {customers.map((customer) => {
            return (
              <div key={customer.id} className="display_search_result">
                <SearchCustomerAccordion
                  customer={customer}
                  redirectHandler={redirectToDetailsPage}
                />
              </div>
            );
          })}
        </div>
      )}
    </React.Fragment>
  );
};

export default SearchResults;
