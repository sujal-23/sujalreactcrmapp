import React from "react";
import SearchResults from "./CustomerSearch/SearchResults";
import SearchForm from "./CustomerSearch/SearchForm";

function HomePage(props) {
  return (
    <React.Fragment>
      <div className="row">
        <div className="col-lg-12">
          <SearchForm />
          <SearchResults />
        </div>
      </div>
    </React.Fragment>
  );
}
export default HomePage;
