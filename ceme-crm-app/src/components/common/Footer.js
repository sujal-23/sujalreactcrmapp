const { NavLink } = require("react-router-dom");

const Footer = () => {
  return (
    <footer className="footer mt-auto py-3">
      <div className="container">
        <span className="text-muted">
          <p>
            &copy; 2020-2021 Allstate Company, Inc. &middot;
            <NavLink to="#">Privacy</NavLink> &middot;
            <NavLink to="#">Terms</NavLink>
          </p>
        </span>
      </div>
    </footer>
  );
};

export default Footer;
