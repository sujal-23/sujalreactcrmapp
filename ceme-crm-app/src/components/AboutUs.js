import React from "react";

function AboutPage(props) {
	return (
		<div className="jumbotron">
			<div className="row">
				<div className="titleOnly">About Ceme CRM</div>
			</div>
			<div className="row paragraphText">
				We SERVE YOU BETTER.<sup>©</sup>
			</div>

			<div className="row">
				<p>
					This web-based CRM is used to satisfy customers and expand their
					market mostly used by small businesses with a focus on customer.
				</p>
			</div>
		</div>
	);
}

export default AboutPage;
