import actions from "./../actions/actionTypes";

export const CRM_REDUCER_INITIAL_STATE = {
	saveStatus: false,
	loading: false,
	error: false,
};

const saveReducer = (state = CRM_REDUCER_INITIAL_STATE, action) => {
	switch (action.type) {
		case actions.SAVE_CUSTOMER_SUCCESS:
			return {
				...state,
				saveStatus: action.payload,
				loading: false,
			};
		case actions.SAVE_CUSTOMER_FAILURE:
			return {
				...state,
				error: action.payload,
			};
		default: {
			return state;
		}
	}
};

export default saveReducer;
